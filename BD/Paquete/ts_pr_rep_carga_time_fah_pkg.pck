create or replace package xxbol.ts_pr_rep_carga_time_fah_pkg authid current_user is

  /*======================================================================+
  |                 Copyright (c) 2015 Evol Per�                          |
  |                 ORACLE Applications Comun Customizing                 |
  +=======================================================================+
  |                                                                       |
  | DESCRIPTION : Reporte de cargas por times                             |
  |                                                                       |
  | HISTORY                                                               |
  | WHEN          WHO                 WHAT                                |
  | ----------    ------------------- ------------------------------------|
  | 14/12/2014    Frank Alvino        Creaci�n                            |
  +=======================================================================*/
  type cyc_result_h is record(
    request_id          number(15),
    status_code         varchar2(240),
    program             varchar2(483),
    fecha_inicio        date,
    fecha_fin           date,
    segundos            number,
    minutos             number,
    horas               number,
    priority_request_id number(15),
    argument_text       varchar2(240),
    phase_code          varchar2(240));

  TYPE cyc_result_h_cursor IS REF CURSOR RETURN cyc_result_h;

  procedure pr_Reporte(pd_fecha_ejec     in date,
                       pv_fecha          in varchar2,
                       pv_origen         in varchar2,
                       pv_lote           in varchar2,
                       cyc_result_h_data IN OUT cyc_result_h_cursor);
end;
/
create or replace package body xxbol.ts_pr_rep_carga_time_fah_pkg is

  /*======================================================================+
  |                 Copyright (c) 2015 Evol Per�                          |
  |                 ORACLE Applications Comun Customizing                 |
  +=======================================================================+
  |                                                                       |
  | DESCRIPTION : Reporte de cargas por times                             |
  |                                                                       |
  | HISTORY                                                               |
  | WHEN          WHO                 WHAT                                |
  | ----------    ------------------- ------------------------------------|
  | 14/12/2014    Frank Alvino        Creaci�n                            |
  +=======================================================================*/
  procedure pr_Reporte(pd_fecha_ejec     in date,
                       pv_fecha          in varchar2,
                       pv_origen         in varchar2,
                       pv_lote           in varchar2,
                       cyc_result_h_data IN OUT cyc_result_h_cursor) as
  
    vn_max_req_ini number;
    vn_max_req_2   number;
  
  begin
    --Obtenemos el m�ximo id del proceso inicial
    begin
      select max(a.priority_request_id)
        into vn_max_req_ini
        from apps.fnd_conc_req_summary_v a
       where a.HAS_SUB_REQUEST = 'N'
            --and a.PHASE_CODE = 'C'
         and trunc(a.REQUEST_DATE) >= pd_fecha_ejec
         AND a.argument_text LIKE
             '%' || pv_fecha || '%' || pv_origen || '%' || pv_lote || '%'
         and program_short_name = 'TS_XLA_COPIA_SFTP';
    
      dbms_output.put_line('vn_max_req_ini ' || vn_max_req_ini);
    end;
    --Obtenemos el m�ximo id del segundo proceso
    begin
      select max(a.priority_request_id)
        into vn_max_req_2
        from apps.fnd_conc_req_summary_v a
       where a.HAS_SUB_REQUEST = 'N'
            --and a.PHASE_CODE = 'C'
         and trunc(a.REQUEST_DATE) >= pd_fecha_ejec
         AND a.argument_text LIKE '%' || pv_fecha || '%' || pv_origen || '%' ||
             pv_lote || '%' || vn_max_req_ini;
    
      dbms_output.put_line('vn_max_req_2 ' || vn_max_req_2);
    end;
  
    ---REPORTE DE CONCURRENTES POR HORAS
    begin
      OPEN cyc_result_h_data FOR
        SELECT request_id,
               status_code,
               program,
               fecha_inicio,
               fecha_fin,
               segundos,
               minutos,
               horas,
               priority_request_id,
               argument_text,
               phase_code
          FROM (select a.request_id,
                       a.program,
                       a.ACTUAL_START_DATE FECHA_INICIO,
                       a.ACTUAL_COMPLETION_DATE FECHA_FIN,
                       (a.ACTUAL_COMPLETION_DATE - a.ACTUAL_START_DATE) * 24 * 60 * 60 segundos,
                       round(((a.ACTUAL_COMPLETION_DATE -
                             a.ACTUAL_START_DATE) * 24 * 60),
                             2) minutos,
                       round(((a.ACTUAL_COMPLETION_DATE -
                             a.ACTUAL_START_DATE) * 24),
                             2) horas,
                       a.priority_request_id,
                       a.argument_text,
                       a.status_code,
                       a.phase_code
                  from apps.fnd_conc_req_summary_v a
                 where a.PRIORITY_REQUEST_ID in
                       (vn_max_req_ini, vn_max_req_2)
                   and a.HAS_SUB_REQUEST = 'N'
                --and a.PHASE_CODE = 'C'
                UNION ALL
                select a.request_id,
                       a.program,
                       a.ACTUAL_START_DATE FECHA_INICIO,
                       a.ACTUAL_COMPLETION_DATE FECHA_FIN,
                       (a.ACTUAL_COMPLETION_DATE - a.ACTUAL_START_DATE) * 24 * 60 * 60 segundos,
                       round(((a.ACTUAL_COMPLETION_DATE -
                             a.ACTUAL_START_DATE) * 24 * 60),
                             2) minutos,
                       round(((a.ACTUAL_COMPLETION_DATE -
                             a.ACTUAL_START_DATE) * 24),
                             2) horas,
                       a.priority_request_id,
                       a.argument_text,
                       a.status_code,
                       a.phase_code
                  from apps.fnd_conc_req_summary_v a
                 where exists
                 (select 1
                          from (select fcr.request_id, sysdate creation_date
                                  from apps.FND_CONCURRENT_REQUESTS    fcr,
                                       apps.FND_CONCURRENT_PROGRAMS_VL fcpv
                                 where fcr.concurrent_program_id =
                                       fcpv.CONCURRENT_PROGRAM_ID
                                   and fcr.parent_request_id in
                                       (select fcr.request_id
                                          from apps.FND_CONCURRENT_REQUESTS    fcr,
                                               apps.FND_CONCURRENT_PROGRAMS_VL fcpv
                                         where exists
                                         (select 1
                                                  from apps.fnd_conc_req_summary_v a
                                                 where a.HAS_SUB_REQUEST = 'N'
                                                      --and a.PHASE_CODE = 'C'
                                                   and trunc(a.REQUEST_DATE) >=
                                                       pd_fecha_ejec
                                                   AND a.argument_text LIKE
                                                       '%' || vn_max_req_2 /*vn_max_req_ini*/
                                                       || '%'
                                                   and a.concurrent_program_id =
                                                       48121
                                                   and a.REQUEST_ID =
                                                       fcr.parent_request_id)
                                           and fcr.concurrent_program_id =
                                               fcpv.CONCURRENT_PROGRAM_ID
                                           and fcpv.CONCURRENT_PROGRAM_NAME =
                                               'XLAACCUP' -- Programa Contable
                                        )
                                
                                union
                                select fcr.request_id, sysdate creation_date
                                  from apps.FND_CONCURRENT_REQUESTS    fcr,
                                       apps.FND_CONCURRENT_PROGRAMS_VL fcpv
                                 where exists
                                 (select 1
                                          from apps.fnd_conc_req_summary_v a
                                         where a.HAS_SUB_REQUEST = 'N'
                                              --and a.PHASE_CODE = 'C'
                                           and trunc(a.REQUEST_DATE) >=
                                               pd_fecha_ejec
                                           AND a.argument_text LIKE
                                               '%' || vn_max_req_2 /*vn_max_req_ini*/
                                               || '%'
                                           and a.concurrent_program_id = 48121
                                           and a.REQUEST_ID =
                                               fcr.parent_request_id)
                                   and fcr.concurrent_program_id =
                                       fcpv.CONCURRENT_PROGRAM_ID) ar
                         where ar.request_id = a.request_id)
                   and a.HAS_SUB_REQUEST = 'N'
                union all
                select a.request_id,
                       a.program,
                       a.ACTUAL_START_DATE FECHA_INICIO,
                       a.ACTUAL_COMPLETION_DATE FECHA_FIN,
                       (a.ACTUAL_COMPLETION_DATE - a.ACTUAL_START_DATE) * 24 * 60 * 60 segundos,
                       round(((a.ACTUAL_COMPLETION_DATE -
                             a.ACTUAL_START_DATE) * 24 * 60),
                             2) minutos,
                       round(((a.ACTUAL_COMPLETION_DATE -
                             a.ACTUAL_START_DATE) * 24),
                             2) horas,
                       a.priority_request_id,
                       a.argument_text,
                       a.status_code,
                       a.phase_code
                  from apps.fnd_conc_req_summary_v a
                 where a.HAS_SUB_REQUEST = 'N'
                      --and a.PHASE_CODE = 'C'
                   AND exists
                 (select 1
                          from apps.fnd_conc_req_summary_v h
                         where h.HAS_SUB_REQUEST = 'N'
                              -- and h.PHASE_CODE = 'C'
                           and trunc(h.REQUEST_DATE) >= pd_fecha_ejec
                           AND h.argument_text LIKE '%' || vn_max_req_2 /*vn_max_req_ini*/
                               || '%'
                           and h.concurrent_program_id = 48121
                           and h.REQUEST_ID = a.request_id)
                   and a.concurrent_program_id = 48121)
         order by request_id;
    end;
  end pr_Reporte;

end;
/

/* Cambio de prueba 01-07-2021 */

Hola Mundo